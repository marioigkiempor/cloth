#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>

#define LA_IMPLEMENTATION
#include "la.h"

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 640

#define HEX_COLOR(u32_color)                                                   \
    (uint8_t)(u32_color >> 24), (uint8_t)(u32_color >> 16),                    \
        (uint8_t)(u32_color >> 8), (uint8_t)(u32_color >> 0)

// --- SDL Checker functions, quit the application on any SDL error
int scc(int ret_code) {
    if (ret_code < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "[SDL Error]\n\t%s",
                     SDL_GetError());
        exit(1);
    }

    return ret_code;
}

void *scp(void *ret_ptr) {
    if (!ret_ptr) {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "[SDL Error]\n\t%s",
                     SDL_GetError());
        exit(1);
    }

    return ret_ptr;
}

typedef struct POINT_STRUCT {
    V2f pos;
    V2f last_pos;

    int pinned;
} Point;

#define MAX_POINT_COUNT 10000
typedef struct POINT_LIST_STRUCT {
    Point points[MAX_POINT_COUNT];
    int count;
} Point_List;

int point_list_add(Point_List *point_list, Point point) {
    if (point_list->count == MAX_POINT_COUNT - 1) {
        return -1;
    }

    point_list->points[point_list->count] = point;

    return point_list->count++;
}

Point *point_list_get(Point_List *point_list, int index) {
    if (index < 0 || MAX_POINT_COUNT <= index) {
        return 0;
    }

    return &point_list->points[index];
}

float point_dist(Point *p0, Point *p1) {
    V2f d = v2f_sub(p1->pos, p0->pos);
    return v2f_len(d);
}

float gravity = 0.05f;
float friction = 0.999f;
float bounce_dampening = 0.9f;

void point_update(Point *point, float dt) {
    if (point->pinned)
        return;

    (void)dt; // TODO: Start using dt
    V2f vel = v2f_mul(v2f_sub(point->pos, point->last_pos), v2ff(friction));

    point->last_pos = point->pos;

    point->pos = v2f_sum(point->pos, vel);
    point->pos = v2f_sum(point->pos, v2f(0.0f, gravity));
}

void point_constrain(Point *point, float dt) {
    if (point->pinned)
        return;

    (void)dt; // TODO: Start using dt
    V2f vel = v2f_mul(v2f_sub(point->pos, point->last_pos), v2ff(friction));

    if (WINDOW_WIDTH < point->pos.x) {
        point->pos.x = WINDOW_WIDTH;
        point->last_pos.x = point->pos.x + vel.x * bounce_dampening;
    } else if (point->pos.x < 0) {
        point->pos.x = 0;
        point->last_pos.x = point->pos.x + vel.x * bounce_dampening;
    }

    if (WINDOW_HEIGHT < point->pos.y) {
        point->pos.y = WINDOW_HEIGHT;
        point->last_pos.y = point->pos.y + vel.y * bounce_dampening;
    } else if (point->pos.y < 0) {
        point->pos.y = 0;
        point->last_pos.y = point->pos.y + vel.y * bounce_dampening;
    }
}

void point_draw(SDL_Renderer *renderer, Point *point) {
    filledCircleRGBA(renderer, (int)floorf(point->pos.x),
                     (int)floorf(point->pos.y), 2, HEX_COLOR(0xFFFFFFFF));
}

typedef struct STICK_STRUCT {
    Point *p0;
    Point *p1;
    float length;

    int hidden;
} Stick;

#define MAX_STICK_COUNT 20000
typedef struct STICK_LIST_STRUCT {
    Stick sticks[MAX_STICK_COUNT];
    int count;
} Stick_List;

int stick_list_add_len(Stick_List *stick_list, Point *p0, Point *p1,
                       float len) {
    if (stick_list->count == MAX_STICK_COUNT - 1) {
        return -1;
    }

    stick_list->sticks[stick_list->count] =
        (Stick){.p0 = p0, .p1 = p1, .length = len, .hidden = 0};

    return stick_list->count++;
}

int stick_list_add(Stick_List *stick_list, Point *p0, Point *p1) {
    return stick_list_add_len(stick_list, p0, p1, point_dist(p0, p1));
}

void stick_update(Stick *s, float dt) {
    (void)dt; // TODO: Start using dt

    V2f stick_centre = v2f_div(v2f_sum(s->p0->pos, s->p1->pos), v2ff(2.0f));
    V2f stick_dir = v2f_div(v2f_sub(s->p0->pos, s->p1->pos),
                            v2ff(v2f_len(v2f_sub(s->p0->pos, s->p1->pos))));

    if (!s->p0->pinned) {
        s->p0->pos =
            v2f_sum(stick_centre,
                    v2f_mul(stick_dir, v2ff(s->length / 2.0f)));
    }

    if (!s->p1->pinned) {
        s->p1->pos =
            v2f_sub(stick_centre,
                    v2f_mul(stick_dir, v2ff(s->length / 2.0f)));
    }
}

void stick_draw(SDL_Renderer *renderer, Stick *s) {
    if (s->hidden == 0)
        thickLineRGBA(renderer, s->p0->pos.x, s->p0->pos.y, s->p1->pos.x,
                      s->p1->pos.y, 1, HEX_COLOR(0xDDDDDDFF));
}

int main(int argc, char *argv[]) {
    (void)argc;
    (void)argv;

    // NOTE: SDL Initialisation
    scc(SDL_Init(SDL_INIT_VIDEO));
    SDL_Window *window = scp(SDL_CreateWindow(
        "Cloth Simulation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN));

    SDL_Renderer *renderer = scp(SDL_CreateRenderer(
        window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED));

    scc(TTF_Init());

    const char *font_path = "./res/font.ttf";
    TTF_Font *font;
    scp((font = TTF_OpenFont(font_path, 32)));

    // NOTE: Point initialisation
    Point_List point_list = {0};
    Stick_List stick_list = {0};

    int grid_width = 64;
    int grid_height = 64;
    int cell_width = WINDOW_WIDTH / grid_width;
    int cell_height = WINDOW_HEIGHT / grid_height;

    for (int x = 0; x < grid_width; ++x) {
        for (int y = 0; y < grid_height; ++y) {
            Point p = {
                .pos = v2f(x * cell_width, y * cell_height),
                .last_pos = v2f(x * cell_width, y * cell_height),
            };

            if (y == 0 && (x % 10) == 0) {
                p.pinned = 1;
            }

            point_list_add(&point_list, p);
        }
    }

    for (int x = 0; x < grid_width; ++x) {
        for (int y = 0; y < grid_height; ++y) {
            Point *p0 = point_list_get(&point_list, x + y * grid_width);
            Point *p1 = point_list_get(&point_list, (x + 1) + y * grid_width);
            Point *p2 = point_list_get(&point_list, x + (y + 1) * grid_width);

            if (x < grid_width - 1) {
                stick_list_add_len(&stick_list, p0, p1, cell_width);
            }

            if (y < grid_height - 1) {
                stick_list_add_len(&stick_list, p0, p2, cell_height);
            }
        }
    }

    int running = 1;
    uint32_t last = SDL_GetTicks();

    while (running == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT: {
                running = 0;
                break;
            }

            case SDL_MOUSEBUTTONDOWN: {
                int x, y;
                SDL_GetMouseState(&x, &y);

                break;
            }

            case SDL_KEYDOWN: {
                switch (event.key.keysym.sym) {
                case SDLK_q:
                case SDLK_ESCAPE: {
                    running = 0;
                    break;
                }
                }
            }
            }
        }

        // --- Update
        uint32_t now = SDL_GetTicks();
        float dt = (float)(now - last) / 1000.0f;

        for (int point_index = 0; point_index < point_list.count;
             ++point_index) {
            Point *p = &point_list.points[point_index];
            point_update(p, dt);
        }

        for (int physics_iter = 0; physics_iter < 5; ++physics_iter) {
            for (int stick_index = 0; stick_index < stick_list.count;
                 ++stick_index) {
                Stick *s = &stick_list.sticks[stick_index];
                stick_update(s, dt);
            }

            for (int point_index = 0; point_index < point_list.count;
                 ++point_index) {
                Point *p = &point_list.points[point_index];
                point_constrain(p, dt);
            }
        }

        last = now;

        // --- Rendering
        SDL_SetRenderDrawColor(renderer, 0x33, 0x33, 0x55, 0xFF);
        SDL_RenderClear(renderer);

        for (int point_index = 0; point_index < point_list.count;
             ++point_index) {
            Point *p = &point_list.points[point_index];
            point_draw(renderer, p);
        }

        for (int stick_index = 0; stick_index < stick_list.count;
             ++stick_index) {
            Stick *s = &stick_list.sticks[stick_index];
            stick_draw(renderer, s);
        }

        SDL_RenderPresent(renderer);
        /* SDL_Delay(2000); */
    }

    TTF_Quit();
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
